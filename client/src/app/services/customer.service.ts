import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { Customer } from '../models/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private httpClient: HttpClient) { }

  getCustomer(id: Number | String): Observable<any> {
    return this.httpClient.get<any>(`http://localhost:1337/api/customers?id=${id}&populate=*`);
  }

  getRankingsOfCustomer(id: Number): Observable<any> {
    return this.httpClient.get<any>(`http://localhost:1337/api/athletes?id=${id}&populate=*`);
  }

  addCustomer(customer: Customer, jwt: string): Observable<any> {
    return this.httpClient.post<any>('http://localhost:1337/api/customers', {data: customer}, {
      headers: {
        Authorization: `Bearer ${jwt}`,
      }
    });
  }
}
