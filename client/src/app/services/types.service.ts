import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TypesService {

  constructor(private httpClient: HttpClient) { }

  getTypes(): Observable<any> {
    return this.httpClient.get<any>('http://localhost:1337/api/types').pipe(
      tap((response) => {
        response.data.map((item: any) => {
          Object.keys(item.attributes).forEach((attribute) => {
            item[attribute] = item.attributes[attribute]
          })
          delete item['attributes']
        });
        return response.data;
      })
    )
  }
}
