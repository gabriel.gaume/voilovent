import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Ranking } from '../models/ranking';
import { AthletesService } from './athletes.service';

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  constructor(private httpClient: HttpClient, private athletesService: AthletesService) { }

  getRanking(id: String): Observable<any> {
    return this.httpClient.get('http://localhost:1337/api/rankings/' + id + '?populate=*').pipe(
      tap(async (response: any) => {

        response['id'] = response.data.id

        await new Promise((resolve, reject) => {
          this.athletesService.getAthlete(response.data.attributes.athlete.data.id).subscribe(athlete => {
            response['athlete'] = athlete
            resolve(response)
          }, err => reject(err));
        });



        //response['athlete'] = response.data.attributes.athlete.data.attributes
        response['timer'] = response.data.attributes.timer
        response['competition'] = response.data.attributes.competition.data
        response['rank'] = response.data.attributes.rank

        return response;
      })
    )
  }

  addRanking(ranking: Ranking) {
    return this.httpClient.post('http://localhost:1337/api/rankings', { data :ranking})
  }

  deleteRanking(id: String):Observable<any> {
    return this.httpClient.delete('http://localhost:1337/api/rankings/' + id )
  }
}
