import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnPreviousComponent } from './return-previous.component';

describe('ReturnPreviousComponent', () => {
  let component: ReturnPreviousComponent;
  let fixture: ComponentFixture<ReturnPreviousComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnPreviousComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnPreviousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
