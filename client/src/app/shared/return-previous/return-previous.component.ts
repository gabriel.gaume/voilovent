import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-return-previous',
  templateUrl: './return-previous.component.html',
  styleUrls: ['./return-previous.component.scss']
})
export class ReturnPreviousComponent implements OnInit {

  constructor(private location: Location) {}

  ngOnInit(): void {
  }
  
  backClicked() {
    this.location.back();
  }

}
