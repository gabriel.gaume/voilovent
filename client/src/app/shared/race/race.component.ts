import { Component, Input, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Ranking } from 'src/app/models/ranking';
import { AthletesService } from 'src/app/services/athletes.service';
import { CompetitionsService } from 'src/app/services/competitions.service';
import { CustomerService } from 'src/app/services/customer.service';
import { RankingService } from 'src/app/services/rankings.service';
@Component({
  selector: 'app-race',
  templateUrl: './race.component.html',
  styleUrls: ['./race.component.scss']
})
export class RaceComponent implements OnInit {
  @Input() race = {} as any;
  @Input() past = false;
  @Input() admin = false;

  registered: Boolean = false;

  ranking: any;

  userId: String = this.cookieService.get('userID');

  constructor(private cookieService: CookieService, private customerService: CustomerService, private rankingService: RankingService, private competitionService: CompetitionsService) { }

  ngOnInit(): void {
    if(this.userId) {
      this.customerService.getCustomer(this.userId).subscribe((response) => {
        let athleteId : String  = response.data[0].attributes.athlete.data.id

        this.race.rankings.data.forEach((ranking: any) => {

          this.rankingService.getRanking(ranking.id).subscribe((res) => { 
            if(athleteId === res.data.attributes.athlete.data.id) {
              this.registered = true;
            } else {
              this.registered = false;
            }
          })
         
        });
      })
    }
  }

  join() {
    if(this.userId) {
      this.customerService.getCustomer(this.userId).subscribe((response) => {
        let athleteId : String  = response.data[0].attributes.athlete.data.id

        this.competitionService.getCompetitionForRanking(this.race.id).subscribe((competition) => {
          if(competition.rankings.data && competition.rankings.data.length !== 0) {
            console.log(competition.rankings)
            competition.rankings.data.map((ranking: any) => {
              if (ranking.attributes.athlete.data.id === athleteId) {
                this.rankingService.deleteRanking(ranking.id).subscribe(rankingRes => {
                  this.registered = false;
                });
              }
            })
          } else {
            this.ranking = {
              athlete: athleteId,
              competition: this.race.id 
            }
            this.rankingService.addRanking(this.ranking).subscribe(rankingRes =>  {
              this.registered = true;
            })
            
          }
        })
      
      }, err => console.log(err)
      )
    }
  }

}
