import { Component, Input, OnInit } from '@angular/core';
import { Ranking } from 'src/app/models/ranking';

@Component({
  selector: 'app-podium',
  templateUrl: './podium.component.html',
  styleUrls: ['./podium.component.scss']
})
export class PodiumComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      console.log(this.podium)
    }, 500);
  }

  @Input() podium: Ranking[] = [];

  // podium = [
  //   {
  //     firstName: 'John',
  //     lastName: 'Doe',
  //     score: 38,
  //   },
  //   {
  //     firstName: 'John',
  //     lastName: 'Doe',
  //     score: 38,
  //   },
  //   {
  //     firstName: 'John',
  //     lastName: 'Doe',
  //     score: 38,
  //   },
  // ]

}
