import { Component, OnInit, Input } from '@angular/core';
import { Competition } from '../../models/competition';
import { CompetitionsService } from '../../services/competitions.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Ranking } from 'src/app/models/ranking';


@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss']
})
export class CompetitionComponent implements OnInit {

  constructor(private competitionsService: CompetitionsService, private route: ActivatedRoute) { }

  competition: Competition;

  rankings: Ranking[] = [];
  podium: Ranking[] = [];

  ngOnInit(): void {
    this.getCompetition(this.route.snapshot.params['id']);
  }

  getCompetition(id: String) {
    this.competitionsService.getCompetition(id).subscribe(response => {
      this.competition = response;
      this.rankings = response.rankings;
      setTimeout(() => {
        this.rankings = this.rankings.sort((a: Ranking, b: Ranking) => {
          return (a.rank < b.rank) ? -1 : 1;
        });
        this.podium = this.rankings.splice(0,3);
      }, 100);
      //this.podium = response.rankings
    })
  }

}
