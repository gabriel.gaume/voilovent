import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionsPastPageComponent } from './competitions-past-page.component';

describe('CompetitionsPastPageComponent', () => {
  let component: CompetitionsPastPageComponent;
  let fixture: ComponentFixture<CompetitionsPastPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompetitionsPastPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionsPastPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
