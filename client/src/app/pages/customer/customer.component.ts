import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { Customer } from '../../models/customer';
import { Ranking } from '../../models/ranking';
import { Athlete } from '../../models/athlete';
import { CompetitionsService } from '../../services/competitions.service';
import { Competition } from '../../models/competition';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  customer : Customer = {
    id: 0,
    firstName: '',
    lastName: '',
    photo: '',
  };

  competNumber = 0;

  currentCustomer = this.route.snapshot.params['id'];

  rankings: Ranking[] = [];

  athlete: Athlete = {
    isPro: false,
    boat: '',
  };

  constructor(private customerService: CustomerService, private competitionsService: CompetitionsService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getCustomer();
  }

  getCustomer() {
    this.customerService.getCustomer(this.currentCustomer).subscribe(response => {
      this.customer.firstName = response.data[0].attributes.firstName;
      this.customer.lastName = response.data[0].attributes.lastName;
      this.customer.photo = response.data[0].attributes.photo;
      this.athlete.isPro = response.data[0].attributes.athlete.data.attributes.isPro;
      this.athlete.boat = response.data[0].attributes.athlete.data.attributes.boat;
      this.athlete.customer = this.customer;
      this.customer.athlete = this.athlete;
      this.getRankings(response.data[0].attributes.athlete.data.id);
    });
  }

  getRankings(id: Number) {
    this.customerService.getRankingsOfCustomer(id).subscribe(response => {
        response.data[0].attributes.rankings.data.forEach((element : any) => {
          this.competNumber = this.competNumber + 1;
          this.competitionsService.getCompetition(element.id).subscribe(response => {
            let newCompetition : Competition = {
              id: response.data.id,
              name: response.data.attributes.name,
              type: response.data.attributes.type,
              place: response.data.attributes.place,
              dateStart: response.data.attributes.dateStart,
              dateEnd: response.data.attributes.dateEnd,
              isPro: response.data.attributes.isPro,
            }
            this.rankings.push({
              id: element.id,
              timer: element.attributes.timer,
              rank: element.attributes.rank,
              competition: newCompetition,
            })
          })
        });
    })
  }

}
