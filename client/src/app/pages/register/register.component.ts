import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Customer } from '../../models/customer';
import { User } from '../../models/user';
import { CustomerService } from '../../services/customer.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup | any;
  submitted = false;
  customer: Customer;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private customerService: CustomerService
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
    });
  }

  register(email:string, password:string, username:string):Observable<{jwt: string, user: User}> {
    return this.httpClient.post('http://localhost:1337/api/auth/local/register', {
      username: username,
      email: email,
      password: password,
    }) as Observable<{jwt: string, user: User}>;
  }
  
  linkCustomer(firstname:string, lastname:string, user:User, jwt:string) {
    this.customer = {
      id: user.id,
      firstName: firstname,
      lastName: lastname,
      photo: '../../../assets/images/1.jpg',
      user: user.id,
    }

    this.customerService.addCustomer(this.customer, jwt).subscribe();
  }

  onSubmit() {
    this.submitted = true;

    const formValues = [
      this.form.value.email,
      this.form.value.password,
      this.form.value.username,
      this.form.value.firstname,
      this.form.value.lastname,
    ]

    this.register(formValues[0],formValues[1],formValues[2]).subscribe(
      res => this.linkCustomer(formValues[3], formValues[4], res.user, res.jwt),
      err => console.log('HTTP Error', err),
    );

    if (this.form.invalid) {
        return;
    } else {
      location.href = "";
    }
  }
}
