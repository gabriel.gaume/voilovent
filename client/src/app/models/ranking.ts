import { Athlete } from "./athlete";
import { Competition } from "./competition";

export interface Ranking {
    id: String,
    timer: Date,
    rank: Number,
    athlete?: Athlete,
    competition?: Competition
}
