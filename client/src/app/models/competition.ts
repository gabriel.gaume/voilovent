import { Type } from '../models/type'
import { Ranking } from './ranking'

export interface Competition {
    id: String,
    name: String,
    place: String,
    dateStart: Date,
    dateEnd: Date,
    isPro: Boolean,
    type: Type
    rankings?: Array<Ranking>
    lat?: number,
    long?: number,
}
