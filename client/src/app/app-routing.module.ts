import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompetitionComponent } from './pages/competition/competition.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AdminComponent } from './pages/admin/admin.component';
import { HomeComponent } from './pages/home/home.component';
import { CustomerComponent } from './pages/customer/customer.component';
import { RegisterComponent } from './pages/register/register.component';
import { CompetitionsPageComponent } from './pages/competitions-page/competitions-page.component';
import { CompetitionsPastPageComponent } from './pages/competitions-past-page/competitions-past-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { PlanComponent } from './pages/plan/plan.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'customer',
    children: [
      {
        path: ':id',
        component: CustomerComponent,
      },
    ]
  },
  {
    path: 'competitions',
    component: CompetitionsPageComponent,
  },
  {
    path: 'competitions/past',
    component: CompetitionsPastPageComponent,
  },
  {
    path: 'competition',
    children: [
      {
        path: ':id',
        component: CompetitionComponent
      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'admin',
    component: AdminComponent,
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'plan',
    component: PlanComponent,
  },
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
