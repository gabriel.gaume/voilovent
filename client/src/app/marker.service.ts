import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as L from 'leaflet';
import { PopUpService } from './popup.service';
import { Competition } from './models/competition';

@Injectable({
  providedIn: 'root'
})

export class MarkerService {
  competitionsService: any;
  limit = true;
  competitions: Competition[] = [];

  constructor(
    private http: HttpClient,
    private popupService: PopUpService) {
  }

  makeCapitalMarkers(map: L.Map | L.LayerGroup<any>, competitions: any[]): void {
    competitions.forEach((competition: { lat: number; long: number; id: String; }) => {
      if(competition.lat && competition.long){
        const circle = L.circleMarker([competition.long, competition.lat], { radius: 10 }).addTo(map);
        circle.bindPopup(this.popupService.makeCapitalPopup(competition.id));
        console.log(competition.id)
      }
    });
  }
}

function Input() {
  throw new Error('Function not implemented.');
}
